# Sagemath Example 

Example on how to use the [Sagemath dev-container](https://gitlab.com/lujobi-projects/miscellaneous/cryptography/dev-image).

## How to start developing 

1) Open in remote container.
2) start the sagemath notebook: `./run.sh`
3) Then click on the link looking as follows:
![link image](assets/Link.png)